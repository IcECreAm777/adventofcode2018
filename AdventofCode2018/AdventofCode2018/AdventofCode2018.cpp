// AdventofCode2018.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include <iostream>
#include "Day1.h"
#include "Day2.h"
#include "Day3.h"
#include "Day4.h"

void ExecuteDay1();
void ExecuteDay2();
void ExecuteDay3();
void ExecuteDay4();

int main()
{
    std::cout << "Advent Of Code 2018\n\n";
	//ExecuteDay1();
	//ExecuteDay2();
	//ExecuteDay3();
	ExecuteDay4();
	std::cout << std::endl;
}

void ExecuteDay1()
{
	Day1 d1;
	std::cout << "Day 1 - Frequencies\n";
	std::cout << d1.Task1() << std::endl;
	std::cout << d1.Task2() << std::endl << std::endl;
}

void ExecuteDay2()
{
	Day2 d;
	std::cout << "Day 2 - Checksum\n";
	std::cout << d.Task1() << std::endl;
	std::cout << d.Task2() << std::endl << std::endl;
}

void ExecuteDay3()
{
	Day3 d;
	std::cout << "Day 3 - Overlapping\n";
	std::cout << d.Task1() << std::endl;
	std::cout << d.Task2() << std::endl << std::endl;
}

void ExecuteDay4()
{
	Day4 d;
	std::cout << "Day 4 - Guards\n";
	std::cout << d.Task1() << std::endl;
	std::cout << d.Task2() << std::endl << std::endl;
}

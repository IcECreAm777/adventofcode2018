#include "pch.h"
#include "Day4.h"


Day4::Day4()
{
	INPUT = SortInput();
}


Day4::~Day4()
{
}

std::string Day4::Task1()
{
	int id = 0, minute = 0, result = 0;
	std::map<int, std::vector<int>> guards = GetGuardSleepingMinutes();
	GetSavestTime(guards, id, minute);
	result = id * minute;
	return Base::Task1() + std::to_string(result);
}

std::string Day4::Task2()
{
	return Base::Task2();
}

std::vector<std::vector<std::string>> Day4::SortInput()
{
	std::vector<std::vector<std::string>> output;
	std::ifstream input("Day4-1_Input.txt");
	std::string line;

	while (std::getline(input, line))
	{
		std::vector<std::string> parsedInput;
		parsedInput.push_back(line.substr(6, 5));
		parsedInput.push_back(line.substr(15, 2));
		parsedInput.push_back(line.substr(19));
		output.push_back(parsedInput);
	}

	std::sort(output.begin(), output.end());

	return output;
}

std::map<int, std::vector<int>> Day4::GetGuardSleepingMinutes()
{
	std::map<int, std::vector<int>> guards;

	std::string currentId;
	int sleep;
	for (std::vector<std::string> & entry : INPUT)
	{
		if (entry[2].find('#'))
		{
			currentId = entry[2].substr(entry[2].find('#') + 1);
			currentId = currentId.substr(0, currentId.find_first_not_of('#'));
			
			if (guards.find(std::stoi(currentId)) != guards.end())
			{
				guards.insert(std::pair<int, std::vector<int>>(std::stoi(currentId), std::vector<int>()));
			}
		}
		else if (entry[2].find("falls"))
		{
			sleep = std::stoi(entry[1]);
		} 
		else if(sleep > 0)
		{
			for (int i = sleep; i < sleep + std::stoi(entry[1]); i++)
			{
				guards.at(std::stoi(currentId)).push_back(i);
			}
		}
	}

	return guards;
}

void Day4::GetSavestTime(std::map<int, std::vector<int>>& guards, int & id, int & minute)
{
	int sleptMinutes = 0;
	for (std::pair<const int, std::vector<int>> & entry : guards)
	{
		if (entry.second.size() > sleptMinutes)
		{
			sleptMinutes = entry.second.size();
			id = entry.first;
		}
	}

	std::map<int, int> count;
	for (int & i : guards.at(id))
	{
		count.insert_or_assign(i, count.at(i)++);
	}
	
	for (std::pair<const int, int> & entry : count)
	{
		minute = entry.second > minute ? entry.first : minute;
	}
}

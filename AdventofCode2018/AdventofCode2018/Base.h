#pragma once
#include <string>
#include <fstream>
#include <vector>

class Base
{
public:
	Base();
	~Base();
	virtual std::string Task1();
	virtual std::string Task2();
};


#pragma once
#include "Base.h"

class Day3 :
	public Base
{
public:
	Day3();
	~Day3();
	std::string Task1() override;
	std::string Task2() override;

private:
	std::vector<std::vector<int>> InitFabric();
	std::vector<std::vector<std::string>> GetInputs();
	void ClaimFabric(std::vector<std::vector<int>>& fabric);
	void ParseToCoordinates(std::string corners, std::string area, int& left, int& top, int& width, int& length);
	int GetDoubleClaimed(std::vector<std::vector<int>>& fabric);

	std::vector<std::vector<int>> FABRIC;
	std::vector<std::vector<std::string>> INPUTS;

	const int FABRIC_LENGTH = 1000;
	const int FABRIC_WIDTH = 1000;
};


#pragma once
#include "Base.h"
#include <algorithm>

class Day2 :
	public Base
{
public:
	Day2();
	~Day2();
	std::string Task1() override;
	std::string Task2() override;
};


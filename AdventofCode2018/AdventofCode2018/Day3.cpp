#include "pch.h"
#include "Day3.h"

Day3::Day3()
{
	INPUTS = GetInputs();
	FABRIC = InitFabric();
	ClaimFabric(FABRIC);
}


Day3::~Day3()
{
}

std::string Day3::Task1()
{
	return Base::Task1() + std::to_string(GetDoubleClaimed(FABRIC));
}

std::string Day3::Task2()
{
	std::string id;
	for (std::vector<std::string>& elf : INPUTS)
	{
		bool overlapped = false;
		int left, top, width, length;
		ParseToCoordinates(elf[2], elf[3], left, top, width, length);

		for (int i = left; i < left + width; i++)
		{
			for (int j = top; j < top + length; j++)
			{
				overlapped = FABRIC[i][j] != 1 ? true : overlapped;
			}
		}

		if (!overlapped)
		{
			id = elf[0];
			break;
		}
	}

	return Base::Task2() + id;
}

std::vector<std::vector<int>> Day3::InitFabric()
{
	std::vector<std::vector<int>> output;

	for (int i = 0; i < FABRIC_LENGTH; i++)
	{
		output.push_back(std::vector<int>());
		for (int j = 0; j < FABRIC_WIDTH; j++)
		{
			output[i].push_back(0);
		}
	}

	return output;
}

std::vector<std::vector<std::string>> Day3::GetInputs()
{
	std::vector<std::vector<std::string>> output;
	std::fstream input("Day3-1_Input.txt");
	std::string line;
	int groupIndex = -1;

	while (input >> line)
	{
		if (line.at(0) == '#')
		{
			output.push_back(std::vector<std::string>());
			groupIndex++;
		}
		output[groupIndex].push_back(line);
	}

	return output;
}

void Day3::ClaimFabric(std::vector<std::vector<int>>& fabric)
{
	for (std::vector<std::string>& elf : INPUTS)
	{
		int left, top, width, length;
		ParseToCoordinates(elf[2], elf[3], left, top, width, length);

		for (int i = left; i < left + width; i++)
		{
			for (int j = top; j < top + length; j++)
			{
				fabric[i][j]++;
			}
		}
	}
}

void Day3::ParseToCoordinates(std::string corners, std::string area, int & left, int & top, int & width, int & length)
{
	corners.erase(corners.length() - 1);
	int index = corners.find(',');
	left = std::stoi(corners.substr(0, index));
	top = std::stoi(corners.substr(index + 1, corners.length() - 1));

	index = area.find('x');
	width = std::stoi(area.substr(0, index));
	length = std::stoi(area.substr(index + 1, area.length() - 1));
}

int Day3::GetDoubleClaimed(std::vector<std::vector<int>>& fabric)
{
	int doubled = 0;
	for (int i = 0; i < FABRIC_WIDTH; i++)
	{
		for (int j = 0; j < FABRIC_LENGTH; j++)
		{
			doubled += fabric[i][j] > 1 ? 1 : 0;
		}
	}
	return doubled;
}

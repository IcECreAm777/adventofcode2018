#include "pch.h"
#include "Day1.h"


Day1::Day1()
{
}

Day1::~Day1()
{
}

std::string Day1::Task1()
{
	std::fstream input("Day1-1_Input.txt");
	int result = 0;

	while (input >> line)
	{
		result += line;
	}

	return Base::Task1() + std::to_string(result);
}

std::string Day1::Task2()
{
	bool twice = false;
	int result = 0;
	std::vector<int> passed = std::vector<int>();
	passed.push_back(result);

	while (!twice)
	{
		std::fstream input("Day1-1_Input.txt");
		while (input >> line)
		{
			result += line;

			if (std::find(passed.begin(), passed.end(), result) != passed.end())
			{
				twice = true;
				break;
			}

			passed.push_back(result);
		}
	}

	return Base::Task2() + std::to_string(result);
}

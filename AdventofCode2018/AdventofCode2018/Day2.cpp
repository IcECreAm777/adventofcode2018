#include "pch.h"
#include "Day2.h"


Day2::Day2()
{
}


Day2::~Day2()
{
}

std::string Day2::Task1()
{
	std::fstream input("Day2-1_Input.txt");
	int twice = 0, tripple = 0, result = 0;
	std::string line;

	while (input >> line)
	{
		bool b_twice = true, b_tripple = true;
		std::string copy = line;
		std::sort(copy.begin(), copy.end());
		for (int i = 0; i < copy.length(); i++)
		{
			if (b_tripple && i + 2 < copy.length() && copy.at(i + 2) == copy.at(i))
			{
				i += 2;
				tripple++;
				b_tripple = false;
			}
			else if (b_twice && i + 1 < copy.length() && copy.at(i + 1) == copy.at(i))
			{
				i++;
				twice++;
				b_twice = false;
			}
		}
	}

	result = twice * tripple;
	return Base::Task1() + std::to_string(result);
}

std::string Day2::Task2()
{
	std::fstream input("Day2-1_Input.txt");

	return Base::Task2();
}

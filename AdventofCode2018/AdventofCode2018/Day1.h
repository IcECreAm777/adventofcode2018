#pragma once
#include "Base.h"
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

class Day1 : Base
{
public:
	Day1();
	~Day1();
	std::string Task1() override;
	std::string Task2() override;

private:
	int line = 0;
};


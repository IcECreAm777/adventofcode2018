#pragma once
#include "Base.h"
#include <algorithm>
#include <map>

class Day4 :
	public Base
{
public:
	Day4();
	~Day4();
	std::string Task1() override;
	std::string Task2() override;

private:
	std::vector<std::vector<std::string>> SortInput();
	std::map<int, std::vector<int>> GetGuardSleepingMinutes();
	void GetSavestTime(std::map<int, std::vector<int>>& guards, int & id, int & minute);

	std::vector<std::vector<std::string>> INPUT;
};

